# coding: utf-8
# Importa as bibliotecas PIL (Python Imaging Library),
# os (Biblioteca para usar arquivos),
# shutil (Biblioteca usada para substituir Arquivos),
# time (Biblioteca pra tratar data atual e formatação nas Datas dos Arquivos),
# datetime (Biblioteca para pegar data e hora atual do Sistema)
# pylovepdf (Biblioteca para comprimir arquivos PDF (PAGA), grátis somente 250 PDFs por Mês)
import PIL
from PIL import Image
import os
from shutil import move
import time
from time import strftime
from datetime import datetime
from pylovepdf.ilovepdf import ILovePdf

# Variavel Global para mostrar a quantidade de imagens\documentos em cada linha
cont = 1


class CompressImageDocs:
    # Metodo construtor da Classe, instanciando as 3 variaveis principais (caminhoOrigem = Diretorio das Imagens,
    # manterNome = SE DESEJA SUBSTITUIR O ARQUIVO, OU CRIAR OUTRO COM '_COMPRIMIDO' NA FRENTE),
    # tamanhoMax = Tamanho Maximo dos arquivos para tentar comprimi-los)
    def __init__(self, caminhoOrigem, manterNome, tamanhoMax):
        self.caminhoOrigem = caminhoOrigem
        self.tamanhoMax = tamanhoMax
        self.manterNome = manterNome

    def compressImage(self):
        try:
            # Instancia a variavel Global e inicializa as variaveis usadas no escopo
            global cont
            caminhoArquivo = self.caminhoArquivo
            caminhoDestino = self.caminhoDestino
            manterNome = self.manterNome
            tamanhoMax = self.tamanhoMax

            # Abre a imagem e pega os dados principais dela, nome, tamanho, extensão, etc.
            imagem = Image.open(caminhoArquivo)
            widht, height = imagem.size
            widhtPermitido = 800
            heightPermitido = 600
            extensao = caminhoArquivo.split(
                ".")[len(caminhoArquivo.split(".")) - 1]
            novoNome = caminhoArquivo.split(
                "\\")[len(caminhoArquivo.split("\\")) - 1]
            novoNome = caminhoArquivo.split(
                "\\")[len(caminhoArquivo.split("\\")) - 1]
            novoNome = novoNome.replace(str(".") + str(extensao), "")
            novoNome = novoNome + "_Comprimido." + extensao

            tamanhoOriginal = os.path.getsize(caminhoArquivo)
            tamanhoFinal = 0
            compressNovamente = 1

            # Loop para tentar comprimir mais de uma vez, se a imagem continuar grande apos a primeira compressao
            while(compressNovamente == 1):
                # Verifica as medidas da imagem
                if (widht > widhtPermitido or height > heightPermitido):
                    if(widht > widhtPermitido):
                        height = int(
                            height * ((widhtPermitido * 100) / widht) / 100)
                        widht = widhtPermitido
                    else:
                        widht = int(
                            widht * ((heightPermitido * 100) / height) / 100)
                        height = heightPermitido

                # Salva a imagem, separado por PNG e JPG'S, metodo de reducao diferente
                imagem = imagem.resize((widht, height), PIL.Image.ANTIALIAS)
                if(extensao == "png"):
                    imagem.save(caminhoDestino + novoNome,
                                format="PNG", optimize=True)
                else:
                    imagem.save(caminhoDestino + novoNome,
                                format="JPEG", optimize=True, quality=80)

                # Verifica o tamanho da imagem, e as medidas para se necessario comprimir de novo no Loop
                tamanhoFinal = os.path.getsize(caminhoDestino + novoNome)
                if(tamanhoFinal > tamanhoMax):
                    widhtPermitido -= 80
                    heightPermitido -= 60

                    if (widhtPermitido < 640 or heightPermitido < 480):
                        if(manterNome == "true" and tamanhoFinal > tamanhoOriginal):
                            os.remove(caminhoDestino + novoNome)
                        elif (manterNome == "true" and tamanhoFinal <= tamanhoOriginal):
                            move(caminhoDestino + novoNome, caminhoArquivo)

                        compressNovamente = 0
                    else:
                        compressNovamente = 1
                else:
                    if(manterNome == "true" and tamanhoFinal > tamanhoOriginal):
                        os.remove(caminhoDestino + novoNome)
                    elif (manterNome == "true" and tamanhoFinal <= tamanhoOriginal):
                        move(caminhoDestino + novoNome, caminhoArquivo)

                    compressNovamente = 0

            # Verificacao somente para mostrar o nome correto na saida do codigo
            if(manterNome == "true"):
                nomeArquivo = caminhoArquivo
            else:
                nomeArquivo = caminhoDestino + novoNome

            # Verificacao somente para mostrar a saida do codigo
            if(tamanhoFinal < tamanhoOriginal):
                print(str(cont) + " - Imagem: " + nomeArquivo + " | Tamanho (Antes): " +
                      str(tamanhoOriginal / 1000) + " Kbs - Tamanho (Depois): " + str(tamanhoFinal / 1000) + " Kbs")
            else:
                print(str(cont) + " - Imagem: " + nomeArquivo + " | Tamanho (Antes): " +
                      str(tamanhoOriginal / 1000) + " Kbs - Nao foi alterado")

            # Soma necessaria para mostrar o numero de cada imagem na saida do codigo
            cont = cont + 1
        except:
            print("Ocorreu um problema na Compressão da Imagem")
            return 0

    def compressDocumentPDF(self):
        # Instancia a variavel Global e inicializa as variaveis usadas no escopo
        try:
            global cont
            caminhoArquivo = self.caminhoArquivo
            caminhoDestino = self.caminhoDestino
            manterNome = self.manterNome

            nomeDocumento = os.path.join(caminhoArquivo).split("\\")
            nomeDocumento = nomeDocumento[len(nomeDocumento)-1]

            dataAtual = datetime.now()
            dataAtual = dataAtual.strftime("%d-%m-%Y")
            extensao = nomeDocumento.split(
                ".")[len(nomeDocumento.split(".")) - 1]
            novoNome = nomeDocumento.replace("." + str(extensao), "")
            novoNome = novoNome + "_compress_" + \
                str(dataAtual) + "." + extensao
            tamanhoOriginal = os.path.getsize(caminhoArquivo)
            tamanhoFinal = 0

            # project_public = Token gerado no Site PyLove para a biblioteca funcionar, só gera até 250 PDFs por Mês
            ilovepdf = ILovePdf(
                "project_public_378af5a0bbf54cc2629ada54f95a06b9_CICDT62dca4e6dfb091561d1a1d5a01c83cfc", verify_ssl=True)
            task = ilovepdf.new_task("compress")
            task.add_file(caminhoArquivo)
            task.compression_level = "extreme"
            task.set_output_folder(caminhoDestino)
            task.output_filename = "actrom_comprimir"
            task.execute()
            task.download()
            # task.delete_current_task()

            # Verificacao somente para mostrar o nome correto na saida do codigo
            print(novoNome)
            if(manterNome == "true"):
                nomeArquivo = nomeDocumento
                move(caminhoDestino + "comprimir.pdf_actrom_comprimir.pdf",
                     caminhoArquivo)
            else:
                nomeArquivo = novoNome

            tamanhoFinal = os.path.getsize(caminhoArquivo)
            # Verificacao somente para mostrar a saida do codigo
            if(tamanhoFinal < tamanhoOriginal):
                print(str(cont) + " - Documento (PDF): " + nomeArquivo + " | Tamanho (Antes): " +
                      str(tamanhoOriginal / 1000) + " Kbs - Tamanho (Depois): " + str(tamanhoFinal / 1000) + " Kbs")
            else:
                print(str(cont) + " - Documento (PDF): " + nomeArquivo + " | Tamanho (Antes): " +
                      str(tamanhoOriginal / 1000) + " Kbs - Nao foi alterado")

            # Soma necessaria para mostrar o numero de cada documento na saida do codigo
            cont = cont + 1
        except:
            print("Ocorreu um problema na Compressão do PDF")
            return 0

    # Funcao "Principal" utilizada para comprimir as documentos e salva-las no mesmo diretorio
    def percorreProprioDiretorio(self):
        # Inicializa as variaveis usadas no escopo
        diretorio = self.caminhoOrigem
        tamanhoMax = self.tamanhoMax

        # Loop no diretorio e sub-diretorios passado pelo parametro "caminhoOrigem"
        for r, d, f in os.walk(diretorio):
            for file in f:
                nomeArquivo = os.path.join(r, file)
                extensao = nomeArquivo.split(
                    ".")[len(nomeArquivo.split(".")) - 1]

                caminhoOrigem = os.path.join(r, file).split(
                    "\\")[len(os.path.join(r, file).split("\\")) - 1]
                caminhoOrigem = os.path.join(
                    r, file).replace(caminhoOrigem, "")
                tamanhoOriginal = os.path.getsize(nomeArquivo)

                # Instancia novas variaveis para a funcao "compressImage()" ou "compressDocsPDF", com o self
                self.caminhoArquivo = nomeArquivo
                self.caminhoDestino = diretorio

                # Pega a data de CRIAÇÃO do Arquivo para comparar com a Data Atual
                dataCriacao = os.path.getctime(nomeArquivo)
                dataCriacao = time.gmtime(dataCriacao)
                dataCriacao = strftime("%d/%m/%Y", dataCriacao)
                dataAtual = datetime.now()
                dataAtual = dataAtual.strftime("%d/%m/%Y")

                # Verifica a extensao da documento (se permitida), e a Data de CRIAÇÃO se é igual a Data Atual
                if(extensao == "pdf") and (dataCriacao == dataAtual):
                    CompressImageDocs.compressDocumentPDF(self)
                # Verifica a extensao da imagem (se permitida), o tamanho se e maior que o parametro 'tamanhoMax' e a Data de CRIAÇÃO se é igual a Data Atual
                # and (dataCriacao == dataAtual):
                elif(extensao == "jpg" or extensao == "jpeg" or extensao == "png") and (tamanhoOriginal > tamanhoMax):
                    CompressImageDocs.compressImage(self)


# Instancia a funcao com os parametros e chama a funcao 'comprimirProprioDiretorio()'
CompressImageDocs("C:\\Users\\ACTROM_Cayo\\Downloads\\_data_VPS2GB\\",
                  "true", 200000).percorreProprioDiretorio()
